---
layout: post
title: Design Scalable Systems - Netflix
tags: programming
---

Basic Facts
--------------
1. Provide instant streaming hundreds of thousand HD videos.
- Large distributed databases.
- High load network data flow.
2. A large group of users.
- Massive number of concurrent requests.
- User Data Management.
3. UI
4. APIs

Constraints
--------------

Abstract Design
--------------

Performance
--------------

Tradeoffs
--------------
