---
layout: post
title: Shortcuts to Algorithm Challenges
tags: programming
---

Reference
-----------
For these questions, you can find their original posts in the following websites:<br>
[https://leetcode.com/problemset/algorithms/](https://leetcode.com/problemset/algorithms/) <br>
[http://www.lintcode.com/en/problem/](http://www.lintcode.com/en/problem/) <br>
[https://www.hackerrank.com/](https://www.hackerrank.com/) <br>
[http://www.geeksforgeeks.org/](http://www.geeksforgeeks.org/) <br>
[http://www.programcreek.com/](http://www.programcreek.com/) <br>

Feature ideas
-------------

There are two ways of doing subproblems.

1. **Top-Down** : Start solving the given problem by breaking it down. If you see that the problem has been solved already, then just return the saved answer. If it has not been solved, solve it and save the answer. This is usually easy to think of and very intuitive. This is referred to as `Memoization`.

2. **Bottom-Up** : Analyze the problem and see the order in which the sub-problems are solved and start solving from the trivial subproblem, up towards the given problem. In this process, it is guaranteed that the subproblems are solved before solving the problem. This is referred to as `Dynamic Programming`.

General guide to python regular matching
-----------------
~~~ python
'.' : matches any character except newline.
'^' : matches the start of a string.
'$' : matches the end of a string.
'*' : matches 0 or more repetitions of the preceding RE. i.e. 'ab*' matches a followed with any number of b.
'+' : similar to '*', but 'ab+' will not match 'a'.
'?' : match 0 or 1 of preceding RE.
{m} : matches exactly m copies of preceding RE.
{m, n}: matches number of copies  from m to n.
'\' : escape special chars.
[] : indicate a set of chars.i.e. [amk] will match 'a', 'm' or 'k'.
others : https://docs.python.org/2/library/re.html
~~~
